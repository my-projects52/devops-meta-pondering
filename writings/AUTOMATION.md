<!-- markdownlint-disable -->
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Automation 11.12.2022](#automation-11122022)
    - [Background](#background)
    - [The wonder ponder](#the-wonder-ponder)
    - [Conclusion](#conclusion)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
<!-- markdownlint-enable -->

# Automation 11.12.2022

I have been wondering lately why some people or even organizations are denying
and defying whole automation *mentality* approach.

## Background

My approach to automation is that *we automate everything*. This as a
mentality. We always aim to automation and when we go to actuall work, User
Story or subtask etc, if there is some technical thing what prevents automation
then that small thing we do not automate rest we automate. This approach
has always positive outpout, increased knowledge in automation and in thing
what we are automating. This starts to build practise to do this automatically
(pun intented). Also knowledge why it cannot be autoamted is good information.
Incorporating this to developer workflow has positive inpact also to
development speed, and quality by having faster automated way to do things.

## The wonder ponder

When talking about this *we automate everything* it can cause passionated
responce. *No! we cannot do that, then we cannot do features, it will grind our
velocity to halt.*. \
Like okay but how do we improve, how do we automate? \
*We do it later!!!*

This flow of talk, i have experienced myself, i have heard it from other
*evankelist*. One can also read similar as this fuming writings in Hacker
News, Reddit etc.

## Conclusion

You will not see silver bullet to tackle this. Only what i can say that try
show working flow, show data (State of DevOps reports). Trust yourself.

> It is possible to commit no mistakes and still lose. \
> That is not a weakness, that is life. \
> Jean Luc Picard
