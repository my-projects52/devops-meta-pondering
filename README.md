<!-- markdownlint-disable -->
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [DevOps meta pondering](#devops-meta-pondering)
    - [Test](#test)
    - [Build](#build)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
<!-- markdownlint-enable -->
![Build Status](https://gitlab.com/my-projects52/devops-meta-pondering/badges/master/pipeline.svg)

# DevOps meta pondering

## Test

Three "tests" are used to validate documents and gitbook configuration.
`markdownlint .` and `doctoc --gitlab .` for markdown settins and
`gitbook build` gitbook settings.

## Build

One can locally build using `gitbook serve`.
