#!/usr/bin/env bash

set -euo

doctoc --gitlab .

if [[ $(git status --porcelain) ]]; then
    echo "doctoc changes! please run doctoc . to update readmes"
    exit 1
fi
